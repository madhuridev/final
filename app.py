from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello !! This is a GitOps Illustration-- 2:27 15/4/2020 !!!!"

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000)
